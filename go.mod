module gitlab.com/rsrchboy/gitlab_ciserver_exporter

go 1.14

require (
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/prometheus/client_golang v1.12.1
	github.com/sirupsen/logrus v1.8.1
	github.com/xanzy/go-gitlab v0.66.0
)
