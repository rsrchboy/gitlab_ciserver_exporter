# GitLab CI Server Exporter

Export interesting things we know about CI, but from server-based information.

## Environment variables

* `GITLAB_ENDPOINT` (ex.: `https://gitlab.local/api/v4`)
* `GITLAB_TOKEN`
* `GITLAB_USERNAME`
* `GITLAB_PASSWORD`

## Usage

Usage of `./gitlab_ciserver_exporter`:

    -cache-ttl duration
            Cache TTL for users activity data.
    -log-level string
            Level of logging. (default "INFO")
    -version
            Prints version and exit.
    -web.listen-address string
            Address to listen on for web interface and telemetry. (default ":9775")
    -web.telemetry-path string
            Path under which to expose metrics. (default "/metrics")

## Metrics

* gitlab_ciserver_last_scrape_error
* gitlab_ciserver_runner_info
* gitlab_ciserver_runners_total
* gitlab_ciserver_scrapes_total

## Credits

This software is based on [gitlab_activity_exporter](https://github.com/leominov/gitlab_activity_exporter).
