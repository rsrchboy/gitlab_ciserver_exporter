FROM golang:1.14-alpine3.11 as builder
WORKDIR /go/src/gitlab.com/rsrchboy/gitlab_ciserver_exporter
COPY . .
RUN go build -o gitlab_ciserver_exporter ./

FROM alpine:3.11
COPY --from=builder /go/src/gitlab.com/rsrchboy/gitlab_ciserver_exporter/gitlab_ciserver_exporter /usr/local/bin/gitlab_ciserver_exporter
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

EXPOSE 9775

ENTRYPOINT ["gitlab_ciserver_exporter"]
