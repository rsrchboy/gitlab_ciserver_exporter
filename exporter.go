package main

import (
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/patrickmn/go-cache"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

const namespace = "gitlab_ciserver"

type Exporter struct {
	config    *Config
	gitlabCli *gitlab.Client

	cache    *cache.Cache
	cacheTTL time.Duration

	runnersTotal    prometheus.Gauge
	lastScrapeError prometheus.Gauge
	totalScrapes    prometheus.Counter
	runnerInfo      *prometheus.Desc
}

func NewExporter(c *Config, cacheTTL time.Duration) (*Exporter, error) {

	e := &Exporter{
		// cache:  cache.New(1*time.Minute, 5*time.Minute),
		cache:    cache.New(cacheTTL, 2*cacheTTL),
		config:   c,
		cacheTTL: cacheTTL,
		totalScrapes: prometheus.NewCounter(prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "scrapes_total",
			Help:      "Current total scrapes.",
		}),
		lastScrapeError: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "last_scrape_error",
			Help:      "The last scrape error status.",
		}),
		runnersTotal: prometheus.NewGauge(prometheus.GaugeOpts{
			Namespace: namespace,
			Name:      "runners_total",
			Help:      "Total number of runners.",
		}),
		runnerInfo: prometheus.NewDesc(
			prometheus.BuildFQName(namespace, "runner", "info"),
			"Runner information as known to the server.",
			[]string{
				"id",
				"status",
				"active",
				"is_shared",
				"name",
				"description",
				"ip_address",
				"online",
				// non-basic follow
				"revision",
				"version",
				"platform",
				"access_level",
				"locked",
				"tags",
			},
			nil,
		),
	}
	err := e.setupGitLabClient()
	return e, err
}

func (e *Exporter) setupGitLabClient() error {
	var (
		cli *gitlab.Client
		err error
	)
	if len(e.config.GitlabToken) > 0 {
		cli, err = gitlab.NewClient(e.config.GitlabToken, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	} else {
		cli, err = gitlab.NewBasicAuthClient(e.config.GitlabUsername, e.config.GitlabPassword, gitlab.WithBaseURL(e.config.GitlabEndpoint))
	}
	e.gitlabCli = cli
	return err
}

func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	ch <- e.totalScrapes.Desc()
	ch <- e.lastScrapeError.Desc()
	ch <- e.runnersTotal.Desc()
	ch <- e.runnerInfo
}

func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	e.scrape(ch)

	ch <- e.totalScrapes
	ch <- e.lastScrapeError
	ch <- e.runnersTotal
}

func (e *Exporter) scrape(ch chan<- prometheus.Metric) {
	e.totalScrapes.Inc()
	e.lastScrapeError.Set(0)

	runners, err := e.GetAllRunners()
	if err != nil {
		logrus.WithError(err).Error("Failed to fetch runner list")
		e.lastScrapeError.Set(1)
		return
	}

	e.runnersTotal.Set(float64(len(runners)))

	for _, runner := range runners {

		// "pristine" non-details labels
		// ch <- prometheus.MustNewConstMetric(e.runnerInfo, prometheus.GaugeValue, 1, []string{
		// 	strconv.Itoa(runner.ID),
		// 	runner.Status,
		// 	strconv.FormatBool(runner.Active),
		// 	strconv.FormatBool(runner.IsShared),
		// 	runner.Name,
		// 	runner.Description,
		// 	runner.IPAddress,
		// 	strconv.FormatBool(runner.Online),
		// }...)

		d, _ := e.GetRunnerDetails(runner.ID)

		var lastContactedAt float64 = 0
		if d.ContactedAt != nil {
			lastContactedAt = float64(time.Time(*d.ContactedAt).Unix())
		}
		ch <- prometheus.MustNewConstMetric(e.runnerInfo, prometheus.CounterValue, lastContactedAt, []string{
			strconv.Itoa(runner.ID),
			runner.Status,
			strconv.FormatBool(runner.Active),
			strconv.FormatBool(runner.IsShared),
			runner.Name,
			runner.Description,
			runner.IPAddress,
			strconv.FormatBool(runner.Online),
			// non-basic after this point
			d.Revision,
			d.Version,
			d.Platform,
			d.AccessLevel,
			strconv.FormatBool(d.Locked),
			strings.Join(d.TagList, ","),
		}...)

	}
}

func (e *Exporter) GetRunnerDetails(id int) (*gitlab.RunnerDetails, error) {

	// TODO it'd be neat to have metrics here, too

	if runner, found := e.cache.Get(strconv.Itoa(id)); found {
		return runner.(*gitlab.RunnerDetails), nil
	}

	runner, _, err := e.gitlabCli.Runners.GetRunnerDetails(id)
	if err != nil {
		return nil, err
	}

	// 5min + random offset so we don't slam the server every 5 min
	ttl := time.Duration(rand.Intn(60))*time.Second + *cacheTTL
	e.cache.Set(strconv.Itoa(id), runner, ttl)
	return runner, nil
}

func (e *Exporter) GetAllRunners() ([]*gitlab.Runner, error) {
	var runners []*gitlab.Runner
	page := 1
	for {
		options := &gitlab.ListRunnersOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
			// Active: gitlab.Bool(true),
		}
		fetchedRunners, r, err := e.gitlabCli.Runners.ListAllRunners(options)
		if err != nil {
			return nil, err
		}
		runners = append(runners, fetchedRunners...)
		nextPageRaw := r.Header.Get("X-Next-Page")
		if len(nextPageRaw) == 0 {
			break
		}
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			break
		}
		page = nextPage
	}
	return runners, nil
}
