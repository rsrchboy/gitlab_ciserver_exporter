package main

import (
	"encoding/json"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	GitlabEndpoint string `envconfig:"GITLAB_ENDPOINT" required:"true" default:"https://gitlab.com/api/v4"`
	GitlabToken    string `envconfig:"GITLAB_TOKEN"`
	GitlabUsername string `envconfig:"GITLAB_USERNAME"`
	GitlabPassword string `envconfig:"GITLAB_PASSWORD"`
}

func LoadConfigFromEnv() (*Config, error) {
	c := &Config{}
	return c, envconfig.Process("", c)
}

func (c *Config) String() string {
	b, _ := json.Marshal(c)
	return string(b)
}
